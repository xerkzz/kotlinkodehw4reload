package com.example.xerks.kotlinkodehw4

import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var data: Array<Planet> = arrayOf(
                Planet("xxx", BitmapFactory.decodeResource(resources, R.drawable.earth),
                        999999999, BitmapFactory.decodeResource(resources, R.drawable.human),
                        "https://en.wikipedia.org/wiki/Earth"),
                Planet("yyy", BitmapFactory.decodeResource(resources, R.drawable.jupiter),
                        88888888, BitmapFactory.decodeResource(resources, R.drawable.alien),
                        "https://en.wikipedia.org/wiki/Earth"),
                Planet("zzz", BitmapFactory.decodeResource(resources, R.drawable.saturn),
                        777777777, BitmapFactory.decodeResource(resources, R.drawable.alien),
                        "https://en.wikipedia.org/wiki/Earth"))

        segments.layoutManager = GridLayoutManager(applicationContext, 2)
        segments.adapter = PlanetAdapter(data)
    }
}
