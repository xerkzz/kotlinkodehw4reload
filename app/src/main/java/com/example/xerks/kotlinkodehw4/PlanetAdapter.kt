package com.example.xerks.kotlinkodehw4

/**
 * Created by Xerks on 10.12.2017.
 */
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.segment.view.*

class PlanetAdapter(private val planets: Array<Planet>) : RecyclerView.Adapter<PlanetAdapter.PlanetViewHolder>() {

    override fun getItemCount() = planets.size

    override fun onBindViewHolder(holder: PlanetViewHolder, position: Int) {
        holder.name.text = planets[position].name
        holder.pImg.setImageBitmap(planets[position].img)
        holder.distance.text = String.format((planets[position].distance ).toString())
        holder.iImg.setImageBitmap(planets[position].live)
        holder.view.setOnClickListener({
            val webIntent = Intent(holder.view.context, WebActivity::class.java)
            webIntent.putExtra(Intent.EXTRA_TEXT, planets[position].sourse)
            holder.view.context.startActivity(webIntent)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PlanetViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.segment, parent, false))

    class PlanetViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.planet_name
        var pImg: ImageView = view.planet_img
        var distance: TextView = view.planet_distance
        var iImg: ImageView = view.planet_live
        var sourse: TextView = view.sourse_link
    }
}